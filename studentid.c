#include <mudlib.h>

inherit OBJECT;

void create()
{
  set("name", "學生證");
  set("short", "學生證(Student ID)");
  set("long", @ItemDesc
這是一張魔法學院的學生證，學生姓名模模糊糊的看不清楚。
這張學生證似乎有著特別的魔法，照片也黃黃的，在學生證的
下方有條碼（Barcode），似乎也有特別的功用。
ItemDesc
  );
  set("id", ({ "student id", "student ID", "學生證" }) );
  set("mass", 1);
  set("bulk", 1);
  set("unit", "張");
}